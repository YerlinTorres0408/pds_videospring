package com.pds.springbd;

import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.pds.beans.Equipo;
import com.pds.beans.Jugador;
import com.pds.beans.Marca;
import com.pds.service.ServiceJugador;
import com.pds.service.ServiceMarca;


@Component
public class App {

	public static void main(String[] args) {
		
		
		
		
		
		ApplicationContext appContext= new ClassPathXmlApplicationContext("com/pds/xml/beans.xml");
	
		
		
		ServiceJugador sm=(ServiceJugador)appContext.getBean("serviceJugadorImpl");
	   
		//Equipo eq1=(Equipo)appContext.getBean("equipo1");
		Jugador jugador=(Jugador)appContext.getBean("jugador1");
		
		try {
			sm.registrar(jugador);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	
	
	}

}
