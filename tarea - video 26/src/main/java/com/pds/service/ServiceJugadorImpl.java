package com.pds.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pds.beans.Jugador;
import com.pds.dao.DAOJugador;


@Service
public class ServiceJugadorImpl implements ServiceJugador {

	@Autowired
	private DAOJugador daojugador;
	public void registrar(Jugador ju) throws Exception {
		
		try {
			daojugador.registrar(ju);
		} catch (Exception e) {
			throw e;
		}
		
	}

}
