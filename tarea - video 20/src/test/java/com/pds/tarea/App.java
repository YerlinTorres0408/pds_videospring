package com.pds.tarea;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.pds.beans.Barcelona;
import com.pds.beans.Ciudades;
import com.pds.beans.Jugador;
import com.pds.beans.Persona;
import com.pds.interfaces.IEquipo;

public class App {

	public static void main(String[] args) {
		
		ApplicationContext appContext= new ClassPathXmlApplicationContext("com/pds/xml/beans.xml");
		
		//Jugador m=(Jugador) appContext.getBean("messi");
	//Barcelona bar= (Barcelona)appContext.getBean("barcelona");
		
		Jugador m=(Jugador)appContext.getBean("messi");
		
        //System.out.println(m.getNombre()+ "-" + m.getEquipo().mostrar());
	//System.out.println(bar.mostrar());
		System.out.println(m.getNombre()+"-"+m.getEquipo().mostrar());
        
        ((ConfigurableApplicationContext)appContext).close();//cierra bean
	}

}
