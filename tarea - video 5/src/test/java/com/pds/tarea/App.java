package com.pds.tarea;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.pds.beans.AppConfig;
import com.pds.beans.AppConfig2;
import com.pds.beans.Mundo;

public class App {

	public static void main(String[] args) {
		
		//ApplicationContext appContext= new ClassPathXmlApplicationContext("com/pds/xml/beans.xml");
		AnnotationConfigApplicationContext appContext= new AnnotationConfigApplicationContext();
		appContext.register(AppConfig.class);
		appContext.register(AppConfig2.class);
		appContext.refresh();
		
		Mundo m=(Mundo) appContext.getBean("marte");
        System.out.println(m.getSaludo());
        
        
        ((ConfigurableApplicationContext)appContext).close();
	}

}
