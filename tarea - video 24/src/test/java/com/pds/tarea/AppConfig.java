package com.pds.tarea;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.pds.beans.Barcelona;
import com.pds.beans.Camiseta;
import com.pds.beans.Jugador;
import com.pds.beans.Juventus;
import com.pds.beans.Marca;





@Configuration
public class AppConfig {
	
	@Bean
	public Jugador jugador1(){
		return new Jugador();
	}
	
	@Bean
	public Barcelona barcelona(){
		return new Barcelona();
	}
	@Bean
	public Barcelona barcelona1(){
		return new Barcelona();
	}
	
	@Bean
	public Juventus juventus(){
		return new Juventus();
	}
	
	@Bean
	public Camiseta camiseta(){
		return new Camiseta();
	}
	
	@Bean
	public Marca marca(){
		return new Marca();
	}
    
}
