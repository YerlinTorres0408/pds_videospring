package com.pds.tarea;

import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.pds.beans.Barcelona;
import com.pds.beans.Ciudades;
import com.pds.beans.Jugador;
import com.pds.beans.Juventus;
import com.pds.beans.Persona;
import com.pds.interfaces.IEquipo;


@Component
public class App {

	public static void main(String[] args) {
		
		Scanner sc= new Scanner(System.in);
		System.out.println("Elija un equipo: 1- BARCELONA 2-JUVENTUS");
		
		int respuesta=sc.nextInt();
		
        ApplicationContext appContext= new AnnotationConfigApplicationContext(AppConfig.class);
		Jugador m=(Jugador)appContext.getBean("jugador1");
		
		switch(respuesta){
		case 1:
			m.setEquipo(new Barcelona());
			break;
		case 2:
			m.setEquipo(new Juventus());
			break;
		default:
		    break;
		}
		
		System.out.println(m.getNombre()+"-"+m.getEquipo().mostrar()+ "-"+ m.getEquipo().mostrar()+"-"+m.getCamiseta().getNumero()+"-"+m.getCamiseta().getMarca().getNombre());
        
        ((ConfigurableApplicationContext)appContext).close();//cierra bean
	}

}
