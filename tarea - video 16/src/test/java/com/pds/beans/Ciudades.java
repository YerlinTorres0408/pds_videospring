package com.pds.beans;

public class Ciudades {
	
	private String nombre;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	private void initBean(){
		System.out.println("Antes de incializar bean");
	}
	private void destroyBean(){
		System.out.println("Bean a punto de ser destruido");
	}
	

}
