package com.pds.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pds.beans.Marca;
import com.pds.dao.DAOMarca;


@Service
public class ServiceMarcaImpl implements ServiceMarca{
	@Autowired
	private DAOMarca daomarca;

	public void registrar(Marca marca) throws Exception {
		
		try {
			daomarca.registrar(marca);
		} catch (Exception e) {
			throw e;
		}
	}

	
	
	
	
}
