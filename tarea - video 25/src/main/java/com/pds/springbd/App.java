package com.pds.springbd;

import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;


import com.pds.beans.Jugador;
import com.pds.beans.Marca;
import com.pds.service.ServiceMarca;


@Component
public class App {

	public static void main(String[] args) {
		
		Marca mar= new Marca();
		
		mar.setId(2);
		mar.setNombre("Marca2");
		
		
		
		ApplicationContext appContext= new ClassPathXmlApplicationContext("com/pds/xml/beans.xml");
	
		
		
		ServiceMarca sm=(ServiceMarca)appContext.getBean("serviceMarcaImpl");
	   
		try {
			sm.registrar(mar);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	
	
	}

}
