package com.pds.dao;

import com.pds.beans.Marca;

public interface DAOMarca {
	
	public void registrar(Marca marca) throws Exception;

}
