package com.pds.tarea;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.pds.beans.Ciudades;
import com.pds.beans.Persona;

public class App {

	public static void main(String[] args) {
		
		ApplicationContext appContext= new ClassPathXmlApplicationContext("com/pds/xml/beans.xml");
		
		Persona m=(Persona) appContext.getBean("persona");
		String nombresCiudades="";
		Ciudades ciu=(Ciudades) appContext.getBean("ciudades");
	
		
        System.out.println(m.getApodo());
        System.out.println(m.getNombre());
        
        
        ((ConfigurableApplicationContext)appContext).close();//cierra bean
	}

}
