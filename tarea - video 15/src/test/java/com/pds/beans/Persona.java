package com.pds.beans;

import javax.annotation.*;

public class Persona {

	
	
	private int id;
	private String nombre;
	private String apodo;
	private Pais pais;
	private Ciudades ciudades;
	
	@PostConstruct
	private void initBean(){
		System.out.println("Antes de incializar bean");
	}
	
	@PreDestroy
	private void destroyBean(){
		System.out.println("Bean a punto de ser destruido");
	}

	
	public Ciudades getCiudades() {
		return ciudades;
	}

	public void setCiudades(Ciudades ciudades) {
		this.ciudades = ciudades;
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApodo() {
		return apodo;
	}

	public void setApodo(String apodo) {
		this.apodo = apodo;
	}
	
	
}
