package com.pds.tarea;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.pds.beans.AppConfig;
import com.pds.beans.AppConfig2;
import com.pds.beans.Persona;

public class App {

	public static void main(String[] args) {
		
		ApplicationContext appContext= new ClassPathXmlApplicationContext("com/pds/xml/beans.xml");
		
		
		
		Persona m=(Persona) appContext.getBean("persona");
        System.out.println(m.getId() +"  "+ m.getNombre()+ "  " + m.getApodo() + "  " + m.getPais().getNombre() + "  " + m.getPais().getNombre());
        
        
        ((ConfigurableApplicationContext)appContext).close();
	}

}
