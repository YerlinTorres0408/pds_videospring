package com.pds.tarea;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.pds.beans.Ciudades;
import com.pds.beans.Persona;

public class App {

	public static void main(String[] args) {
		
		ApplicationContext appContext= new ClassPathXmlApplicationContext("com/pds/xml/beans.xml");
		
		Persona m=(Persona) appContext.getBean("persona");
		String nombresCiudades="";
		Persona m2=(Persona) appContext.getBean("persona");
		
		
		m.setId(1);
		m.setNombre("yerlin");
		m.setApodo("mitocode");
		//singleton = misma referenciA igual id 
		//PROTOTYPE = ID DIFERENTES diferentes instancias
		//lazy-init mejora rendimiento, bean factory trae lazy tru default se usa solo en singleton
        System.out.println(m.getId() +"  "+ m.getNombre()+ "  " + m.getApodo() + "  " + m.getPais().getNombre() + "  " + m.getCiudades().getNombre()+" ");
        System.out.println(m2.getId() +"  "+ m2.getNombre()+ "  " + m2.getApodo() + "  " + m2.getPais().getNombre() + "  " + m2.getCiudades().getNombre()+" ");
       // System.out.println(m);
       // System.out.println(m2);
        
        ((ConfigurableApplicationContext)appContext).close();
	}

}
